document.getElementById("btn_taoDiv").onclick = function () {
  var nhapDiv =document.getElementById('txt_nhapdiv').value*1
  var chuoiDiv = "";
  for (var i = 0; i <= nhapDiv; i++) {
    if (i % 2 == 0) {
      chuoiDiv += `<div class="bg-danger text-white p-2 mt-2 w-75">Div chẵn ${i}</div>`;
      console.log("i chẵn: ", i);
    } else {
      chuoiDiv += `<div class="bg-primary text-white p-2 mt-2 w-75">Div lẽ ${i}</div>`;
      console.log("i lẽ: ", i);
    }
  }
  document.getElementById("result").innerHTML = chuoiDiv;
};
